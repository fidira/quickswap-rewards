import { ethers } from "ethers";

import "dotenv/config";

export const mnemonic = process.env.MNEMONIC || "";
export const privateKey = process.env.PRIVATE_KEY || "";
export const etherscanAPIKey = process.env.ETHERSCAN_API_KEY;
export const reportGas: boolean = true;
export const uniswapV2Router02 = "0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff";

export const REWARDS_RATE = ethers.utils.parseEther("0.01");

export interface networkConfigItem {
  url: string;
  fidira: string;
  stakingDualRewards: string;
  tokenABestPath: string[];
  tokenBBestPath: string[];
  rewardsReserve?: string;
}

export interface networkConfigInfo {
  [key: string]: networkConfigItem;
}

export const networkConfig: networkConfigInfo = {
  localhost: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    stakingDualRewards: "0x3c1f53fed2238176419F8f897aEc8791C499e3c8",
    tokenABestPath: [
      "0xf28164A485B0B2C90639E47b0f377b4a438a16B1",
      "0x831753DD7087CaC61aB5644b308642cc1c33Dc13",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    tokenBBestPath: [
      "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
  },
  hardhat: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    stakingDualRewards: "0x3c1f53fed2238176419F8f897aEc8791C499e3c8",
    tokenABestPath: [
      "0xf28164A485B0B2C90639E47b0f377b4a438a16B1",
      "0x831753DD7087CaC61aB5644b308642cc1c33Dc13",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    tokenBBestPath: [
      "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
  },
  // mumbai will not work because there is no dual rewards pool or dQuick
  // contracts deployed.
  mumbai: {
    url: process.env.MUMBAI_URL || "",
    fidira: "0xCD9305369e04D2BB76e6022Edd2a8cB5cce40162",
    stakingDualRewards: "0x3c1f53fed2238176419F8f897aEc8791C499e3c8", //MATIC:WETH
    tokenABestPath: [
      "0xf28164A485B0B2C90639E47b0f377b4a438a16B1",
      "0x831753DD7087CaC61aB5644b308642cc1c33Dc13",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    tokenBBestPath: [
      "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    rewardsReserve: process.env.REWARD_RESERVE
  },
  polygon: {
    url: process.env.POLYGON_URL || "",
    fidira: "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    stakingDualRewards: "0xc0eb5d1316b835F4B584B59f922d9c87cA5053E5", // USDT:MATIC
    tokenABestPath: [
      "0xf28164A485B0B2C90639E47b0f377b4a438a16B1",
      "0x831753DD7087CaC61aB5644b308642cc1c33Dc13",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    tokenBBestPath: [
      "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
      "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
      "0x9A4Eb698e5DE3D3Df0a68F681789072DE1E50222",
    ],
    rewardsReserve: process.env.REWARD_RESERVE
  },
};

export interface testConfigInfo {
  stakingDualRewards: string;
  fidira: string;
  weth: string;
  uniswapV2Router02: string;
  tokenABestPath: string[];
  tokenBBestPath: string[];
  rewardTokenA: string;
  rewardTokenB: string;
}

/**
 * Provides only the config required for testing. Saves us having to import
 * the entire networkConfig just for testing.
 */
export const testConfig: testConfigInfo = {
  stakingDualRewards: networkConfig.hardhat.stakingDualRewards,
  fidira: networkConfig.hardhat.fidira,
  weth: "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
  uniswapV2Router02: uniswapV2Router02,
  tokenABestPath: networkConfig.hardhat.tokenABestPath,
  tokenBBestPath: networkConfig.hardhat.tokenBBestPath,
  rewardTokenA: "0xf28164a485b0b2c90639e47b0f377b4a438a16b1",
  rewardTokenB: "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270",
};
