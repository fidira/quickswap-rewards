import { ethers, waffle } from "hardhat";
import { Contract, BigNumber } from "ethers";
import { getTwentyMinuteDeadline } from "../deadline";
import {
  getUniswapV2Factory,
  getUniswapV2Router02,
} from "../contracts/periphery";
import { testConfig } from "../../../config";
import ArtifactIERC20 from "@uniswap/v2-core/build/IERC20.json";

/**
 * Fund liquidity pool (E.g. matic/eth) using wallet. Will be owned by deployer.
 */
export const fundMATICETHPool = async (amountIn: BigNumber) => {
  const deployer = waffle.provider.getSigner();

  const router = getUniswapV2Router02();

  const [, amountB] = await router.getAmountsOut(amountIn, [
    router.WETH(),
    testConfig.weth,
  ]);

  await router.swapExactETHForTokens(
    amountB,
    [router.WETH(), testConfig.weth],
    deployer.getAddress(),
    getTwentyMinuteDeadline(),
    { value: amountIn }
  );

  const weth = new Contract(
    testConfig.weth,
    JSON.stringify(ArtifactIERC20.abi),
    deployer
  );

  const balance = await weth.balanceOf(deployer.getAddress());

  await weth.approve(router.address, balance);

  const [lpAmountA, lpAmountB] = await router.getAmountsOut(amountIn, [
    await router.WETH(),
    testConfig.weth,
  ]);

  await router.addLiquidityETH(
    testConfig.weth,
    lpAmountB,
    lpAmountB,
    lpAmountA.sub(lpAmountA.mul(1).div(100)), // sub 1% slippage
    deployer.getAddress(),
    getTwentyMinuteDeadline(),
    { value: lpAmountA }
  );
};

export const getMATICETHPair = async () => {
  const deployer = waffle.provider.getSigner();
  const router = getUniswapV2Router02();
  const factory = await getUniswapV2Factory();
  const pair = await factory.getPair(router.WETH(), testConfig.weth);

  return new Contract(pair, ArtifactIERC20.abi, deployer);
};
