import { waffle, deployments } from "hardhat";
import { Contract } from "ethers";
import { Artifact } from "hardhat/types";
import { testConfig } from "../../../config";
import ArtifactIUniswapV2Router02 from "@uniswap/v2-periphery/build/IUniswapV2Router02.json";
import ArtifactIUniswapV2Factory from "@uniswap/v2-periphery/build/IUniswapV2Factory.json";

export const getUniswapV2Router02 = (): Contract => {
  return new Contract(
    testConfig.uniswapV2Router02,
    JSON.stringify(ArtifactIUniswapV2Router02.abi),
    waffle.provider.getSigner()
  );
};

export const getUniswapV2Factory = async (): Promise<Contract> => {
  const router = getUniswapV2Router02();
  return new Contract(
    await router.factory(),
    JSON.stringify(ArtifactIUniswapV2Factory.abi),
    waffle.provider.getSigner()
  );
};

export const getStakingDualRewards = async (): Promise<Contract> => {
  const ArtfactStakingDualRewardsInterface: Artifact =
    await deployments.getArtifact("StakingDualRewardsInterface");

  return new Contract(
    testConfig.stakingDualRewards,
    ArtfactStakingDualRewardsInterface.abi,
    waffle.provider.getSigner()
  );
};
