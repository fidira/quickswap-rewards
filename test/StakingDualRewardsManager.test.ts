import { expect } from "chai";
import { ethers, waffle, deployments, getNamedAccounts } from "hardhat";
import { Contract, BigNumber } from "ethers";
import { deployMockContract } from "@ethereum-waffle/mock-contract";

import { advanceBlockTimestamp } from "./utils/mining";
import * as periphery from "./utils/contracts/periphery";
import {
  fundMATICETHPool,
  getMATICETHPair,
} from "./utils/liquidity/maticethpool";
import { testConfig } from "../config";

import ArtifactIERC20 from "@uniswap/v2-core/build/IERC20.json";

import { StakingDualRewardsManager } from "../typechain-types";

describe("StakingDualRewardsManager", () => {
  let deployer: any;
  let rewardsReceiver: any;

  let manager: StakingDualRewardsManager;
  let stakingDualRewards: Contract;
  let router: Contract;
  let lp: Contract;

  const amountIn: BigNumber = ethers.utils.parseEther("100");

  before(async () => {
    deployer = waffle.provider.getSigner();

    rewardsReceiver = (await getNamedAccounts()).rewardsReceiver;

    router = periphery.getUniswapV2Router02();
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);
    manager = await ethers.getContract<StakingDualRewardsManager>(
      "StakingDualRewardsManager"
    );

    stakingDualRewards = await periphery.getStakingDualRewards();

    await fundMATICETHPool(amountIn);

    lp = await getMATICETHPair();

    await lp.approve(manager.address, lp.balanceOf(deployer.getAddress()));
  });

  describe("Deposits", () => {
    it("should stake LP tokens into the dual mining pool", async () => {
      const expected = await lp.balanceOf(deployer.getAddress());

      await manager.addLPTokensToPool();

      const stakingDualRewards = await periphery.getStakingDualRewards();

      // StakingDualRewards should mint at 1:1 against LP tokens deposited.
      expect(await stakingDualRewards.balanceOf(manager.address)).to.be.equal(
        expected
      );
    });

    it("should stake additional LP tokens", async () => {
      const balance1 = await lp.balanceOf(deployer.getAddress());

      await manager.addLPTokensToPool();

      await fundMATICETHPool(amountIn);

      const balance2 = await lp.balanceOf(deployer.getAddress());

      await lp.approve(manager.address, lp.balanceOf(deployer.getAddress()));

      await manager.addLPTokensToPool();

      const stakingDualRewards = await periphery.getStakingDualRewards();

      const expected = balance1.add(balance2);

      // StakingDualRewards should mint at 1:1 against LP tokens deposited.
      expect(await stakingDualRewards.balanceOf(manager.address)).to.be.equal(
        expected
      );
    });

    it("should not stake LP tokens if not approved", async () => {
      await lp.approve(manager.address, 0);

      await expect(manager.addLPTokensToPool()).to.be.revertedWith(
        "TransferHelper: TRANSFER_FROM_FAILED"
      );
    });

    describe("Withdrawals", () => {
      beforeEach(async () => {
        await manager.addLPTokensToPool();
        await advanceBlockTimestamp(60);
      });

      it("should withdraw LP tokens to EOA wallet", async () => {
        const expected = await lp.balanceOf(manager.address);

        await manager.withdrawLPTokens();

        expect(await stakingDualRewards.balanceOf(rewardsReceiver)).to.be.equal(
          expected
        );
      });

      it("should withdraw reward token A to EOA wallet", async () => {
        const tokenA = new Contract(
          testConfig.rewardTokenA,
          ArtifactIERC20.abi,
          deployer
        );

        const earnedA = await stakingDualRewards.earnedA(manager.address);

        const expected = earnedA;

        await manager.withdrawLPTokens();

        // rewards are a moving target because they are time-based. If we get
        // get more than the  last earnedB read, we're satisified we're getting
        // decent rewards.
        expect(await tokenA.balanceOf(deployer.getAddress())).to.be.gt(
          expected
        );
      });

      it("should withdraw reward token B to EOA wallet", async () => {
        const tokenB = new Contract(
          testConfig.rewardTokenB,
          ArtifactIERC20.abi,
          deployer
        );

        const earnedB = await stakingDualRewards.earnedB(manager.address);

        const expected = earnedB;

        await manager.withdrawLPTokens();

        // rewards are a moving target because they are time-based. If we get
        // get more than the  last earnedB read, we're satisified we're getting
        // decent rewards.
        expect(await tokenB.balanceOf(deployer.getAddress())).to.be.gt(
          expected
        );
      });
    });
  });

  describe("Rewards", () => {
    it("should check upkeep to distribute rewards", async () => {
      expect((await manager.checkUpkeep([]))[0]).to.be.false;
    });

    it("should perform upkeep to distribute rewards", async () => {
      await manager.addLPTokensToPool();

      const fid = new Contract(testConfig.fidira, ArtifactIERC20.abi, deployer);

      advanceBlockTimestamp(24 * 60 * 60);

      const earnedA = await stakingDualRewards.earnedA(manager.address);
      const earnedB = await stakingDualRewards.earnedB(manager.address);

      await manager.performUpkeep([]);

      const tokenAToFID = await router.getAmountsOut(
        earnedA,
        testConfig.tokenABestPath
      );

      const tokenBToFID = await router.getAmountsOut(
        earnedB,
        testConfig.tokenBBestPath
      );

      const expected = tokenAToFID[tokenAToFID.length - 1].add(
        tokenBToFID[tokenBToFID.length - 1]
      );

      // Because the rewards are converted to fid at the same time, we can't
      // get the tokenA price until after the exchange. This affects the LP
      // price. Therefore, if we're within 1% of the expected amount then
      // we're getting a good return.
      expect(await fid.balanceOf(rewardsReceiver)).to.be.closeTo(
        expected,
        expected.sub(expected.div(100))
      );
    });

    it("should not perform upkeep prematurely", async () => {
      await expect(manager.performUpkeep([])).to.be.revertedWith(
        "StakingDualRewardsManager/checkUpkeep-not-met"
      );
    });

    it("should change rewards pool address", async () => {
      await manager.changeRewardsPool(deployer.getAddress());
      expect(await manager.rewardsPool()).to.be.equal(
        await deployer.getAddress()
      );
    });

    it("should not change rewards pool address if not owner", async () => {
      await expect(
        manager
          .connect(await ethers.getSigner(rewardsReceiver))
          .changeRewardsPool(deployer.getAddress())
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  describe("Zero Balances", async () => {
    let stakingDualRewardsManager: any;

    beforeEach(async () => {
      const stakingDualRewardsMock = await deployMockContract(
        waffle.provider.getSigner(),
        (
          await deployments.getArtifact("StakingDualRewardsInterface")
        ).abi
      );

      stakingDualRewardsMock.mock.exit.returns();
      stakingDualRewardsMock.mock.stakingToken.returns(lp.address);
      stakingDualRewardsMock.mock.rewardsTokenA.returns(
        testConfig.rewardTokenA
      );
      stakingDualRewardsMock.mock.rewardsTokenB.returns(
        testConfig.rewardTokenB
      );
      const StakingDualRewardsManager = await ethers.getContractFactory(
        "StakingDualRewardsManager"
      );

      stakingDualRewardsManager = await StakingDualRewardsManager.deploy(
        stakingDualRewardsMock.address,
        testConfig.uniswapV2Router02,
        testConfig.tokenABestPath,
        testConfig.tokenBBestPath,
        rewardsReceiver
      );
    });

    it("should withdraw lp tokens when rewards are 0", async () => {
      await lp.transfer(
        stakingDualRewardsManager.address,
        lp.balanceOf(deployer.getAddress())
      );

      const expected = await lp.balanceOf(stakingDualRewardsManager.address);

      await stakingDualRewardsManager.withdrawLPTokens();

      expect(await lp.balanceOf(deployer.getAddress())).to.be.equal(expected);
    });

    // @todo should probably check balances of other tokens.
    it("should withdraw other tokens even if LP tokens are 0", async () => {
      await lp.transfer(rewardsReceiver, lp.balanceOf(deployer.getAddress()));

      await stakingDualRewardsManager.withdrawLPTokens();

      expect(await lp.balanceOf(deployer.getAddress())).to.be.equal(0);
    });
  });

  describe("Ownership", async () => {
    it("should have an owner", async () => {
      expect(await manager.owner()).to.be.equal(await deployer.getAddress());
    });

    it("should transfer ownership", async () => {
      await manager.transferOwnership(rewardsReceiver);
      expect(await manager.owner()).to.be.equal(rewardsReceiver);
    });
  });
});
