import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { networkConfig, uniswapV2Router02 } from "../config";

const func: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const accounts = await getNamedAccounts();

  const { deployer, rewardsReceiver } = accounts;

  await deploy("StakingDualRewardsManager", {
    from: deployer,
    args: [
      networkConfig[network.name].stakingDualRewards,
      uniswapV2Router02,
      networkConfig[network.name].tokenABestPath,
      networkConfig[network.name].tokenBBestPath,
      networkConfig[network.name].rewardsReserve || rewardsReceiver,
    ],
    log: true,
  });
};

export default func;
func.tags = ["pool", "contract", "all"];
