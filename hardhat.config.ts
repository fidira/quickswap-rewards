import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "@nomiclabs/hardhat-ethers";
import "solidity-coverage";
import "hardhat-deploy";
import "hardhat-docgen";

import {
  networkConfig,
  reportGas,
  mnemonic,
  privateKey,
  etherscanAPIKey,
} from "./config";

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: "0.8.15",
  networks: {
    polygon: {
      url: networkConfig.polygon.url,
      accounts: [privateKey],
    },
    mumbai: {
      url: networkConfig.mumbai.url,
      accounts: [privateKey],
    },
    hardhat: {
      accounts: {
        mnemonic,
      },
      forking: {
        url: networkConfig.hardhat.url,
        blockNumber: 29760000,
      },
    },
    localhost: {
      accounts: {
        mnemonic,
      },
      forking: {
        url: networkConfig.hardhat.url,
        blockNumber: 29760000,
      },
    },
  },
  gasReporter: {
    enabled: reportGas,
    currency: "USD",
  },
  etherscan: {
    apiKey: etherscanAPIKey,
  },
  namedAccounts: {
    deployer: 0,
    rewardsReceiver: 1,
  },
};

export default config;
