//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

interface StakingDualRewardsInterface {
    function stakingToken() external view returns (address);

    function stake(uint256 amount) external;

    function balanceOf(address account) external view returns (uint256);

    function getReward() external;

    function exit() external;

    function rewardsTokenA() external view returns (address);

    function rewardsTokenB() external view returns (address);

    function earnedA(address account) external view returns (uint256);

    function earnedB(address account) external view returns (uint256);
}
