//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

import "@uniswap/v2-core/contracts/interfaces/IERC20.sol";
import "@uniswap/lib/contracts/libraries/TransferHelper.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@chainlink/contracts/src/v0.8/interfaces/KeeperCompatibleInterface.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./StakingDualRewardsInterface.sol";

/**
 * Manage the staking of liquidity pool tokens in Quickswap's dual rewards pool.
 * @dev The StakingDualRewardsManager doesn't check balances from the rewards
 * pool. If any transactions fail because of no rewards, the manager should be
 * deactivated.
 */
contract StakingDualRewardsManager is KeeperCompatibleInterface, Ownable {
    address public stakingDualRewards;
    address private _uniswapV2Router02;
    address[] private _tokenABestPath;
    address[] private _tokenBBestPath;
    address public rewardsPool;

    uint256 private _interval;
    uint256 public keeperTimestamp;

    constructor(
        address stakingDualRewards_,
        address uniswapV2Router02,
        address[] memory tokenABestPath,
        address[] memory tokenBBestPath,
        address rewardsPool_
    ) {
        stakingDualRewards = stakingDualRewards_;
        _uniswapV2Router02 = uniswapV2Router02;
        _tokenABestPath = tokenABestPath;
        _tokenBBestPath = tokenBBestPath;
        rewardsPool = rewardsPool_;

        _interval = 24 * 60 * 60;
        keeperTimestamp = block.timestamp;
    }

    /**
     * Add LP tokens to the staking dual rewards pool.
     * @dev Let the underlying ERC20 token error if not enough allowance.
     */
    function addLPTokensToPool() external onlyOwner {
        address stakingToken = StakingDualRewardsInterface(stakingDualRewards)
            .stakingToken();

        uint256 amount = IERC20(stakingToken).balanceOf(owner());
        TransferHelper.safeTransferFrom(
            stakingToken,
            owner(),
            address(this),
            amount
        );
        TransferHelper.safeApprove(stakingToken, stakingDualRewards, amount);
        StakingDualRewardsInterface(stakingDualRewards).stake(amount);
    }

    /**
     * @notice Checks whether the upkeep should be run.
     * @return upkeepNeeded True if the upkeep time has elapsed, false
     * otherwise.
     */
    function checkUpkeep(bytes calldata checkData)
        public
        view
        override
        returns (
            bool upkeepNeeded,
            bytes memory /*performData*/
        )
    {
        upkeepNeeded = (block.timestamp - keeperTimestamp) >= _interval;
    }

    /**
     * @notice Perform the upkeep if required.
     * @dev Balances are not checked as LP tokens are assumed to have been
     * deposited and the pool should be generating time-based rewards
     * immediately. If this is not the case, the keeper should be
     * decommissioned.
     */
    function performUpkeep(bytes calldata performData) external override {
        (bool upkeepNeeded, ) = checkUpkeep(performData);

        require(upkeepNeeded, "StakingDualRewardsManager/checkUpkeep-not-met");

        StakingDualRewardsInterface(stakingDualRewards).getReward();

        // swap the more valuable token first for a better rate.

        // Swap token B rewards for FID.
        _transferTokenReward(
            StakingDualRewardsInterface(stakingDualRewards).rewardsTokenB(),
            _tokenBBestPath
        );

        // Swap token A rewards for FID.
        _transferTokenReward(
            StakingDualRewardsInterface(stakingDualRewards).rewardsTokenA(),
            _tokenABestPath
        );

        keeperTimestamp = block.timestamp;
    }

    /**
     * @dev Do not check balances, it's an extraneous condition which costs gas
     * without providing any benefit. Just let the tx fail as the dual pools
     * shouldn't work anyway without both rewards returning something.
     */
    function _transferTokenReward(address token, address[] memory tokenBestPath)
        internal
    {
        uint256 balance = IERC20(token).balanceOf(address(this));

        IUniswapV2Router02 router = IUniswapV2Router02(_uniswapV2Router02);

        uint256[] memory amounts = router.getAmountsOut(balance, tokenBestPath);

        // 1% slippage.
        uint256 minAmountOut = amounts[amounts.length - 1] -
            (amounts[amounts.length - 1] * 1) /
            100;

        TransferHelper.safeApprove(token, _uniswapV2Router02, balance);

        router.swapExactTokensForTokens(
            balance,
            minAmountOut,
            tokenBestPath,
            rewardsPool,
            block.timestamp + 1 * 60
        );
    }

    /**
     * Withdraw the staked tokens along with any rewards.
     */
    function withdrawLPTokens() external onlyOwner {
        StakingDualRewardsInterface(stakingDualRewards).exit();

        uint256 amountRewardsA = IERC20(
            StakingDualRewardsInterface(stakingDualRewards).rewardsTokenA()
        ).balanceOf(address(this));

        if (amountRewardsA > 0) {
            TransferHelper.safeTransfer(
                StakingDualRewardsInterface(stakingDualRewards).rewardsTokenA(),
                owner(),
                amountRewardsA
            );
        }

        uint256 amountRewardsB = IERC20(
            StakingDualRewardsInterface(stakingDualRewards).rewardsTokenB()
        ).balanceOf(address(this));

        if (amountRewardsB > 0) {
            TransferHelper.safeTransfer(
                StakingDualRewardsInterface(stakingDualRewards).rewardsTokenB(),
                owner(),
                amountRewardsB
            );
        }

        address stakingToken = StakingDualRewardsInterface(stakingDualRewards)
            .stakingToken();
        uint256 amountLP = IERC20(stakingToken).balanceOf(address(this));

        if (amountLP > 0) {
            TransferHelper.safeTransfer(stakingToken, owner(), amountLP);
        }
    }

    function changeRewardsPool(address newPool) external onlyOwner {
        address oldPool = rewardsPool;
        rewardsPool = newPool;

        emit RewardsPoolChanged(oldPool, newPool);
    }

    event RewardsPoolChanged(address indexed oldPool, address indexed newPool);
}
